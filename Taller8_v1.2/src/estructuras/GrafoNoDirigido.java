package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Estructuras.TablaHashSC;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	
	private Lista<Nodo<K>> nodos;

	/**
	 * Lista de adyacencia 
	 */
	private TablaHashSC<String, Arco<K,E>[]> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() 
	{
		//TODO implementar
		adj = new TablaHashSC<String, Arco<K,E>[]>();
		nodos = new Lista<Nodo<K>>();
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) 
	{
		//TODO implementar
		nodos.agregar(nodo);
		adj.put(nodo.darId().toString(), darArcosDestino(nodo.darId()));		
		return false;
	}

	@Override
	public boolean eliminarNodo(K id) 
	{
		adj.delete(id.toString());
		//TODO implementar
		return false;
	}

	@Override
	public Arco<K,E>[] darArcos() 
	{
		Arco<K,E>[] rpta;	
		
		return rpta;
		
		//TODO implementar
		
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Lista<Nodo<K>> darNodos() 
	{
		//TODO implementar
		return nodos;
		
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		return false;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
		return null;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		return null;
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
	}

}
